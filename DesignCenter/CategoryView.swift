//
//  CategoryView.swift
//  DesignCenter
//
//  Created by Irko Stoyanov on 31.03.21.
//

import SwiftUI

struct CategoryView: View {
    let categories = ["Kronodesign decor range", "Kronodesign worktop range", "Inspirational Station", " Inspirational Moodboard"]
    var body: some View {
        GeometryReader { g in
            ScrollView {
                VStack {
                    ZStack {
                        Image("office")
                            .resizable()
                            .scaledToFill()
                            .frame(width: g.size.width, height: g.size.height / 3)
                            .brightness(-0.2)
                            .clipped()
                        Text("INTERIOR MATERIALS")
                            .fontWeight(.bold)
                            .foregroundColor(.white)
                            .font(.largeTitle)
                    }
                    Text("Lorem Ipsum Dolor")
                        .font(.title2)
                        .fontWeight(.bold)
                        .padding(.vertical, 20)
                    Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.")
                        .padding(.bottom, 40)
                        .padding(.horizontal, 20)
                        .frame(maxWidth: 750)
                    LazyVGrid(columns: [
                        GridItem(.flexible(minimum: 50, maximum: 360), spacing: g.size.width / 50),
                        GridItem(.flexible(minimum: 50, maximum: 360), spacing: g.size.width / 50),
                        GridItem(.flexible(minimum: 50, maximum: 360), spacing: g.size.width / 50),
                        GridItem(.flexible(minimum: 50, maximum: 360), spacing: g.size.width / 50)
                    ], alignment: .center, spacing: g.size.width / 50, content: {
                        ForEach(categories, id: \.self) { category in
                            VStack(alignment: .leading) {
                                Button(action: {
                                    //todo fix it later
                                    print(category)
                                }, label: {
                                    Text(category)
                                        .font(.caption)
                                        .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                                        .multilineTextAlignment(.center)
                                        .frame(width: g.size.width / 5, height: g.size.width / 5, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                                        
                                })
                                .foregroundColor(.black)
                                .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                            }
                        }
                    })
                }
            }
        }
    }
}

struct CategoryView_Previews: PreviewProvider {
    static var previews: some View {
        CategoryView()
            .previewDevice("iPad Pro (12.9-inch) (4th generation)")
//            .previewDevice("iPhone 12 Pro Max")
    }
}

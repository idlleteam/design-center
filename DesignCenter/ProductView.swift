//
//  ProductView.swift
//  DesignCenter
//
//  Created by Irko Stoyanov on 31.03.21.
//

import SwiftUI

struct ProductView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct ProductView_Previews: PreviewProvider {
    static var previews: some View {
        ProductView()
    }
}

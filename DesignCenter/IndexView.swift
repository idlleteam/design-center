//
//  ContentView.swift
//  DesignCenter
//
//  Created by Irko Stoyanov on 29.03.21.
//

import SwiftUI

struct IndexView: View {
    
    var navigation:[String] = ["Entrance Area", "Windows Displays",
                               "Flooring Area", "Working Area",
                               "Inspirational Furniture", "Interior Materials", "Exterior Materials",
                               "Presentaion Room", "Restrooms",
                               "Smoking Area", "Parking Lot"]
    
    var body: some View {
        GeometryReader { g in
            Text("Design Center Logo")
                .font(.title)
                .fontWeight(.black)
                .frame(minWidth: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/, maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, alignment: .topLeading)
                .padding(.leading, 20)
            HStack {
                Image("scheme")
                    .resizable()
                    .scaledToFit()
                    .frame(maxHeight: 570)
                VStack(alignment: .leading) {
                    ForEach(navigation.indices, id: \.self) { i in
                        let num = i + 1
                        Button(action: {
                            //todo fix it later
                            print(num)
                        }, label: {
                            Text("\(num)")
                                .fontWeight(.bold)
                                .font(.title2)
                                .padding(6)
                                .frame(width: 38, height: 38, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                                .background(Color.black)
                                .foregroundColor(.white)
                                .cornerRadius(60)
                            Text(navigation[i])
                                .font(.title)
                                .foregroundColor(.gray)
                        }).padding(10)
                    }
                }.padding(.leading, 30)
            }.frame(minWidth: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/, idealWidth: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, minHeight: 0, maxHeight: 1000, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            
            Button(action: {
                print("Floating Button Click")
            }, label: {
                NavigationLink(destination: CategoryView()) {
                     Text("Open View")
                 }
            })
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        IndexView()
            .previewDevice("iPad Pro (12.9-inch) (4th generation)")
    }
}

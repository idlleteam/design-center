//
//  DesignCenterApp.swift
//  DesignCenter
//
//  Created by Irko Stoyanov on 29.03.21.
//

import SwiftUI

@main
struct DesignCenterApp: App {
    var body: some Scene {
        WindowGroup {
            IndexView()
        }
    }
}
